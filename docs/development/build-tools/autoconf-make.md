# Build tools

## GNU Autoconf

GNU Autoconf is a tool for producing configure scripts for building,
installing and packaging software on computer systems where a Bourne
shell is available.

At NERSC one should typically replace `./configure` with

```shell
./configure CC=cc CXX=CC FC=ftn F77=ftn
```

in order to use the [compiler wrappers](../compilers/wrappers.md).

It is often useful to see what additional options are available:

```shell
./configure --help | less
```

Examples of common options:

* `--enable-mpi`
* `--enable-hdf5`

## GNU Make

[Make](https://en.wikipedia.org/wiki/Make_(software)) is a common
build automation tool in wide use by Unix like systems. (GNU
Make)[https://www.gnu.org/software/make/] is the most widespread
implementation and the default for Mac OS X and is the default for
most Linux distributions.

A typical Makefile:

```make
TARGET = test
LIBS =
CC = cc
CFLAGS = -g -Wall -qopenmp

.PHONY: default all clean

default: $(TARGET)
all: default

OBJECTS = $(patsubst %.c, %.o, $(wildcard *.c))
HEADERS = $(wildcard *.h)

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $< -o $@

.PRECIOUS: $(TARGET) $(OBJECTS)

$(TARGET): $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -Wall $(LIBS) -o $@

clean:
	-rm -f *.o
	-rm -f $(TARGET)
```

!!! tip
	MPI codes distributed via make have lines like `CC = mpicc`. In
	most cases it is sufficient to change these lines to `CC = cc`.

# Programming Environment Change on Cori in April 2020

## Background

During the scheduled maintenance on Cori on April 22, we will install
the new Cray Programming Environment Software release
[CDT/20.03](https://pubs.cray.com/bundle/Released_Cray_XC_-x86-_Programming_Environments_20.03_March_5_2020/resource/Released_Cray_XC_(x86)_Programming_Environments_20.03_March_5_2020.pdf).
New Intel compiler versions 19.0.8.324 and 19.1.0.166 have already
been installed as of Apr 1.  **There will be no software default
versions changes at this time.**

Below is the detailed list of CDT software changes after the scheduled
maintenance on Cori (April 22).

## New software versions available

* cce/9.1.3
* cray-fftw/3.3.8.5
* cray-hdf5, cray-hdf5-parallel/1.10.6.0
* cray-libsci/20.03.1
* cray-mpich, cray-mpich-abi, cray-shmem/7.7.12
* cray-netcdf, cray-netcdf-hdf5parallel/4.7.3.2
* cray-parallel-netcdf/1.12.0.0
* cray-petsc, cray-petsc-64, cray-petsc-complex, cray-petsc-complex-64/3.12.4.0
* cray-python/3.7.3.2
* cray-tpsl, cray-tpsl-64/20.03.1
* cray-trilinos/12.18.1.0
* craype/2.6.4
* craype-dl-plugin-py3/19.12.1
* gcc/9.2.0
* papi/5.7.0.3
* perftools-base/20.03.0
* pmi, pmi-lib/5.0.15

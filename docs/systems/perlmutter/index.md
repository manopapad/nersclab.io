# Access

Perlmutter is not yet available for general user access. 

## Connecting to Perlmutter

In order to connect to Perlmutter you must connect to
[Cori](../../connect/index.md) or a [DTN](../dtn/#access) and then
connect to Perlmutter as follows:

```
ssh perlmutter
```

Perlmutter will be available to users in several stages, in the
following order:

1. The [NESAP (NERSC Exascale Science Applications
   Program)](https://www.nersc.gov/research-and-development/nesap/)
   tier 1 and [ECP (Exascale Computing
   Project)](https://www.exascaleproject.org) teams
2. The NESAP tier 2 and
   [Superfacility](https://www.nersc.gov/research-and-development/superfacility/)
   teams
3. Selected general users running GPU applications
4. Remaining general users running GPU applications
5. Remaining users

## Preparing for Perlmutter

Please check the [Transitioning Applications to
Perlmutter](../../performance/readiness.md) webpage
for a wealth of useful information on how to transition your
applications for Perlmutter.

## Compiling/Building Software

You can find info below on how to set the proper programming
environment and compile your code on Perlmutter:

-  Compilers at NERSC
    - [Compiler wrappers](../../development/compilers/wrappers.md)
    - [Native compilers](../../development/compilers/native.md)
-  [Using Python on Perlmutter](../../development/languages/python/using-python-perlmutter.md)
-  [Environment](../../environment/index.md)
-  [Lmod](../../environment/lmod.md), a Lua-based module system
   used on Perlmutter
-  [Finding and using software on Perlmutter](software/finding-software.md)

## Running Jobs

Perlmutter uses Slurm for batch job scheduling. Below you can find
info on the queue policies, how to submit jobs using Slurm and
monitor jobs, etc.:

-  [Slurm](../../jobs/index.md)
-  [Queue Policies on Perlmutter](../../jobs/policy.md#perlmutter)
-  [Running Jobs on Perlmutter's GPU nodes](../../jobs/examples/index.md#perlmutter-gpus)
-  [Monitoring Jobs](../../jobs/monitoring.md)
-  [Interactive Jobs](../../jobs/interactive.md)

<!--
To run a job on Perlmutter GPU nodes, you must submit the job using
a project name that ends in `_g` (e.g., `m1234_g`). A project name
without the trailing `_g` is for CPU jobs on Cori and Phase 2
CPU-only nodes.
-->

During Allocation Year 2021 jobs run on Perlmutter will be free of
charge.

## Current Known Issues

[Known Issues on Perlmutter](../../current.md)

# Perlmutter Timeline

This page records a brief timeline of significant events and user
environment changes on Perlmutter.

The following diagram shows a preliminary, approximate timeline for 
Perlmutter installation and access. Please note that dates are estimates
only, and may change.

![Perlmutter_timeline](../images/perlmutter-timeline.png)

## September 10, 2021

- Updated to CPE 21.09. A recompile is recommended but not
  required. Major changes of note include:
     - Upgrade MPICH to 8.1.9 (from 8.1.8)
     - Upgrade DSMML to 0.2.1 (from 0.2.0)
     - Upgrade PALS to 1.0.17 (from 1.0.14)
     - Upgrade OpenSHMEMX to 11.3.3 (from 11.3.2)
     - Upgrade craype to 2.7.10 (from 2.7.9)
     - Upgrade CCE to 12.0.3 (from 12.0.2)
     - Upgrade HDF5 to 1.12.0.7 (from 1.12.0.6)
     - GCC 11.2.0 added     
- Added `cuda` module to the list of default modules loaded at startup
- Set BASH_ENV to Lmod setup file
- Deployed numerous network upgrades and changes intended to increase
  responsiveness and performance
- Performed kernel upgrades to login nodes for better fail over support
- Added latest CMake release as `cmake/git-20210830`, and is set as the default `cmake` on the system

## September 2, 2021

- Updated NVIDIA driver (to
  `nvidia-gfxG04-kmp-default-450.142.00_k4.12.14_150.47-0.x86_64`). This
  is not expected to have any user impact.

## August 30, 2021

### Numerous changes to the NVIDIA programming environment

- Changed default NVIDIA compiler from 20.9 to 21.7
- Installed needed CUDA compatibility libraries
- Added support for multi-CUDA HPC SDK
- Removed the `cudatoolkit` and `craype-accel-nvidia80` modules from default

Tips for users:

- Please use `module load cuda` and `module av cuda` to get the CUDA
  Toolkit, including the CUDA C compiler `nvcc`, and associated
  libraries and tools.
- Cmake may have trouble picking up the correct mpich include
  files. If it does, you can use `set ( CMAKE_CUDA_FLAGS
  "-I/opt/cray/pe/mpich/8.1.8/ofi/nvidia/20.7/include")` to force it
  to pick up the correct one.

## June, 2021

Perlmutter achieved 64.6 Pflop/s, putting it at No. 5 in the [Top500
list](https://www.top500.org/lists/top500/2021/06/).

## May 27, 2021

Perlmutter supercomputer dedication.

## November, 2020 - March, 2021
 
Perlmutter Phase 1 delivered.
